package resultado_teste.Questao01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import resultado_teste.Teste;

/**
 * exercicio Como o anunciado não tem a descrição do como seria a captura das
 * informações, colocarei um limitado informado pelo usuário
 *
 * @author igor
 */
public class Questao01 implements Teste {

    /**
     * metodo que retorna o numero da questao
     *
     * @return o numero da questao
     */
    @Override
    public String getNumeroQuestao() {
        return "01";
    }

    /**
     * metodo que retorna o anunciando
     *
     * @return a questao
     */
    @Override
    public String getAnunciado() {
        return "Escreva um algoritmo que encontre a primeira ocorrência de valor repetido que tenha o menor índice, caso o elemento não seja encontrado deve ser retornado -1";
    }

    /**
     * verifica o numero que mais apresenta e sendo o de menor indice
     *
     * @param numeros
     * @return
     */
    private Object verificar(Object[] numeros) {

        Object valor = -1;

        int count = 0;
        for (int i = 0; i <= numeros.length - 1; i++) { //pecorre para verificar o que mais se repete 
            int q = this.valoresRepetidos(numeros, numeros[i], i + 1);
            if (q > count) {
                valor = numeros[i];
            }

        }
        return valor;
    }

    /**
     * verifica se o valor é repetido
     *
     * @param array
     * @param valor
     * @return retorna a contagem .
     */
    private int valoresRepetidos(Object[] array, Object valor, int indice) {
        int count = 0;
        for (int i = indice; i <= array.length - 1; i++) {
            if (array[i].equals(valor)) {
                count++; // conta o valor  
            }
        }
        return count;
    }

    /**
     * metodo que inicia o teste ;
     */
    @Override
    public void iniciar() {

        System.out.println(this.getNumeroQuestao() + ") " + this.getAnunciado());
        System.out.println("");
        System.out.println("Instrução: Será solicitado o tamanho do array; ");
        System.out.println("Deseja começar? (s/n) ");
        Scanner scan = new Scanner(System.in);
        String linha = scan.nextLine(); //ler a resposta do cliente 
        while (linha.equalsIgnoreCase("s")) { //
            this.executando(); // inicia o teste 
            System.out.println("Deseja começar? (s/n) ");
            linha = scan.nextLine(); //ler a resposta do cliente 
        }
        System.exit(0); // sai do exercicio
    }

    public static void main(String[] args) {

        System.out.println("Começando..: ");

        Questao01 q = new Questao01(); // instanciando a classe da questao

        q.iniciar();  //inicando a questao 

    }

    /**
     * retorna o tamanho do array informado pelo usuário
     *
     * @return
     */
    private int getTamanhoArray() throws Exception {
        System.out.print("Informe o tamanho do array: ");
        Scanner scan = new Scanner(System.in);
        int tamanho = Integer.parseInt(scan.nextLine());
        if (tamanho <= 1) {
            throw new Exception("O tamanho deve ser maior que 1");
        }
        return tamanho;
    }

    /**
     * lendo os dados fornecido pelo usuário
     *
     * `* @param array
     */
    private void lendoArray(Object[] array) {
        Scanner scan = new Scanner(System.in);
        for (int i = 0; i <= array.length - 1; i++) {
            System.out.print("Informe o valor: ");
            String linha = scan.nextLine();
            array[i] = linha;
        }

    }

    /**
     * executando o teste 01
     */
    @Override
    public void executando() {

        Object[] array = null;
        try {
            array = new Object[getTamanhoArray()]; // instancia o array de acordo com tanho fornecido pelo usuário
            this.lendoArray(array); // lendo os dados do array 
            imprimirValor(this.verificar(array)); // verificando o numero 

        } catch (Exception es) {
            System.out.println("" + es.getMessage());
            this.executando();
        }

        String linha = "_";

    }

    /**
     * imprimi o valor
     */
    private void imprimirValor(Object valor) {

        System.out.println(" O valor mais repetidos é: " + valor);
    }

}
