/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resultado_teste.Questao05;

import java.util.Scanner;
import resultado_teste.Teste;

/**
 * implementa a questao 05
 *
 * @author igor
 */
public class Questao05 implements Teste {

    /**
     * metodo que retorna o numero da questao
     *
     * @return o numero da questao
     */
    @Override
    public String getNumeroQuestao() {
        return "05";
    }

    /**
     * metodo que retorna o anunciando
     *
     * @return a questao
     */
    @Override
    public String getAnunciado() {
        return "Fazer um algorítimo que conte a quantidade de ocorrências em uma palavra/frase'.";
    }

    /**
     * iniciar a questao
     */
    @Override
    public void iniciar() {
        System.out.println(this.getNumeroQuestao() + ") " + this.getAnunciado());
        System.out.println("");
        System.out.println("Instrução: O valor não poderá ser nulo");
        System.out.println("Deseja começar? (s/n) ");
        Scanner scan = new Scanner(System.in);
        String linha = scan.nextLine(); //ler a resposta do cliente 
        while (linha.equalsIgnoreCase("s")) { //
            this.executando(); // inicia o teste 
            System.out.println("Deseja começar? (s/n) ");
            linha = scan.nextLine(); //ler a resposta do cliente 
        }
        System.exit(0); // sai do exercicio

    }

    /**
     * executando
     */
    @Override
    public void executando() {
        String frase = this.getFrasePrincipal(); // pega a frase 
        String ocorrencia = this.getOcorrencia(); // pega a ocorrencia
        String[] quebrada = this.estudandoAFrase(frase);
        int quantidade = this.getQuantidade(quebrada, ocorrencia); // contar ;
        System.out.println("Resultado foi: " + quantidade);
    }

    /**
     * verificar numero de ocorrencia
     */
    private String[] estudandoAFrase(String frase) {
        frase = frase.toUpperCase(); // converter tudo para maisculo  para facilitar ;
        String[] strings = frase.split(" ");
        return strings;
    }

    /**
     * retorna a quantidade encontrada
     *
     * @param frases
     * @param ocorrencia
     * @return
     */
    private int getQuantidade(String[] frases, String ocorrencia) {
        int count = 0;
        for (String s : frases) {
            int i = s.indexOf(ocorrencia.toUpperCase());
            if (i > -1) {
                count++;
            }
        }
        return count;
    }

    /**
     * pega a frase ou palavra inicial
     *
     * @return
     */
    private String getFrasePrincipal() {
        System.out.println("Informe a palavra e a frase: ");
        Scanner scan = new Scanner(System.in);
        String frase = scan.nextLine(); //ler a resposta do cliente
        return frase;
    }

    /**
     * pega a ocorrencia que usuario deseja
     *
     * @return
     */
    private String getOcorrencia() {
        System.out.println("Informe a palavra: ");
        Scanner scaner = new Scanner(System.in);
        String ocorrencia = scaner.nextLine(); //ler a resposta do cliente
        return ocorrencia;
    }

    public static void main(String[] args) {

        System.out.println("Começando..: ");

        Questao05 q = new Questao05(); // instanciando a classe da questao

        q.iniciar();  //inicando a questao 
    }

}
