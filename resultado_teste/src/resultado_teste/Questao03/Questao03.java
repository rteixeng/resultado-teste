/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resultado_teste.Questao03;

import java.util.Scanner;

import resultado_teste.Teste;

/**
 * /**
 * questao n 02
 *
 * @author igor
 */
public class Questao03 implements Teste {

    /**
     * metodo que retorna o numero da questao
     *
     * @return o numero da questao
     */
    @Override
    public String getNumeroQuestao() {
        return "03";
    }

    /**
     * metodo que retorna o anunciando
     *
     * @return a questao
     */
    @Override
    public String getAnunciado() {
        return "Escreva um algoritmo que dado uma lista encadeada de números inteiros Y = [ 3, 8, 9, 4, 7, 1, 6, 1]  e K = 1  o resultado final de Y seja = [ 3, 8, 9, 4, 7, 6,]";
    }

    /**
     * metodo que inicia o teste ;
     */
    @Override
    public void iniciar() {
        System.out.println(this.getNumeroQuestao() + ") " + this.getAnunciado());
        System.out.println("");
        System.out.println("Instrução: Sempre escolha opção ");
        System.out.println("Deseja começar? (s/n) ");
        Scanner scan = new Scanner(System.in);
        String linha = scan.nextLine(); //ler a resposta do cliente 
        while (linha.equalsIgnoreCase("s")) { //
            this.executando(); // inicia o teste 
            System.out.println("Deseja começar? (s/n) ");
            linha = scan.nextLine(); //ler a resposta do cliente 
        }
        System.exit(0); // sai do exercicio
    }

    /**
     * executando o teste 03
     */
    @Override
    public void executando() {
        ListaEncadearda lista = new ListaEncadearda();
        String opcao = escolheOpcao(); // captua a opcao 
        while (!opcao.equalsIgnoreCase("0")) { // enquanto a opcao nao for 0 
            switch (opcao) { // caso a opcao  seja 
                case "1": { // incluir um valor apenas no final
                    this.addValorFim(lista); // adiciona o valor a lista 
                    this.exibir(lista); // exibi a lista atualizada 
                    break;
                }
                case "2": {
                    this.addValorInicio(lista); // adiciona o valor no inico da lista 
                    this.exibir(lista); // exibi a lista atualizada
                    break;
                }
                case "3": {
                    this.removerValor(lista); // remove o valor da lista 
                    this.exibir(lista); // ecxibi a lista atualizada
                    break;
                }
                case "4": {
                    this.exibir(lista); // exibi a lista atualizada 
                    break;
                }

            }
            opcao = escolheOpcao();  // captura a opcacao 
        }
    }

    private String escolheOpcao() {
        System.out.println("  ");
        System.out.println("Escolha a opção:  ");
        System.out.println("1) Incluir um valor  "); // sera incluido o valor no fim da lista 
        System.out.println("2) Incluir um valor no inicio  "); // sera incluido o valor no inicio da lista 
        System.out.println("3) Remover um valor da lista   "); // removerá o valor da lista 
        System.out.println("4) Listar    "); // lista 
        System.out.println("0) Sair    "); // sai das opcoes  
        Scanner scan = new Scanner(System.in);
        String opcao = scan.nextLine();
        if (Integer.parseInt(opcao) > 4) {
            System.out.println("Opção inválida");
            this.escolheOpcao();
        }
        return opcao;
    }

    private void removerValor(ListaEncadearda lista) {
        try {
            lista.excluirValor(new NoDaLista(this.getValor()));
        } catch (Exception es) {
            System.out.println(es.getMessage());
        }
    }

    /**
     * pega o valor a ser inserido
     *
     * @return
     */
    private int getValor() {
        System.out.print("Informe o valor:  "); // sera incluido o valor no fim da lista 
        Scanner scan = new Scanner(System.in);
        return Integer.parseInt(scan.nextLine());
    }

    /**
     * adiciona o valor capturado na lista
     *
     * @param lista
     */
    private void addValorFim(ListaEncadearda lista) {
        lista.inserirUltimo(new NoDaLista(this.getValor())); // inseri no fim do valor 
    }

    /**
     * adiciona o valor no inicio
     *
     * @param lista
     */
    private void addValorInicio(ListaEncadearda lista) {
        lista.inserirPrimeiro(new NoDaLista(this.getValor()));
    }

    /**
     * exibi a lista atual
     *
     * @param lista
     *
     */
    private void exibir(ListaEncadearda lista) {
        try {
            System.out.println("A lista atual: ");
            System.out.println(lista.exibirLista());
        } catch (Exception es) {
            System.out.println(es.getMessage().toString());
        }
    }

    public static void main(String[] args) {

        System.out.println("Começando..: ");

        Questao03 q = new Questao03(); // instanciando a classe da questao

        q.iniciar();  //inicando a questao 
    }

}
