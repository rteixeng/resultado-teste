/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resultado_teste.Questao03;

/**
 * baseada na questao por ser uma lista encardeada
 *
 * @author igor
 */
public class ListaEncadearda {

    NoDaLista primeiro, ultimo;  // os nos da lista encardeadas 
    int total; // o total de nos 

    public ListaEncadearda() {
        this.primeiro = this.ultimo = null; // inicia-se os nos com valores nul ;
        this.total = 0;
    }

    /**
     * retorna o total da lista
     *
     * @return
     */
    public int getTotal() {
        return total;
    }

    /**
     * retorna se a lista esta vazia
     *
     * @return
     */
    public boolean isVazia() {
        return (total == 0);
    }

    /**
     * inseri o no no inicio da lista
     */
    public void inserirUltimo(NoDaLista valor) {
        if (isVazia()) // caso seja vazia a lista 
        {
            this.primeiro = this.ultimo = valor; // inseri no primeiro e no ultimo o valor passado 
        } else { // caso  tenha 
            ultimo.proximo = valor; // vincula o ultimo com o proximo valor  
            ultimo = valor; // define que o valor é ultimo agora 
        }
        this.total++; // atualiza o valor total dos nos 
    }

    /**
     * inseri o valor no primeiro
     *
     * @param valor
     */
    public void inserirPrimeiro(NoDaLista valor) {
        if (isVazia()) // verifica se a lista vazia 
        {
            this.primeiro = this.ultimo = valor; // inseri no primeiro e no ultimo o valor passado 
        } else { // caso  tenha 
            valor.proximo = primeiro; // pega o primeiro e coloca vinculado no proximo 
            primeiro = valor; // agora quem é o primeiro é o valor informado
        }
        this.total++; // aturaliza o valor total dos nos 
    }

    /**
     * excluir o valor da lista
     *
     * @param valor
     */
    public void excluirValor(NoDaLista valor) throws Exception {
        NoDaLista valorAntigo; // valor antigo 
        NoDaLista valorAtual; // valor atual da lista 
        valorAntigo = valorAtual = primeiro; // atualiza os valores com o valor do primeiro 
        if (this.total == 1) // a lista precisa esta com 
        {
            throw new Exception("Não é permitida a exclusão da lista quando seu total é um ");
        } else { // caso tenha pelo menos 2 registros 
            int count = 1;
            while (count <= this.total && valorAtual.valor != valor.valor) { // caso o total seja maior e igual ao contador e o valor atual seja difenrete do valor passado 
                valorAntigo = valorAtual; // valor antigo assume o valor total 
                valorAtual = valorAtual.proximo; // atualiza o valor atual com o valor do proximo ; 
                count++; // acrescenta o contador 
            }
            if (valorAtual.valor == this.primeiro.valor) // caso o valor atual seja o valor passado P
            {
                primeiro = valorAtual.proximo; // primeiro recebe o proximo do valor atual 
            } else { // caso nao, o valor antigo do proximo 
                valorAntigo.proximo = valorAtual.proximo;
            }
            this.total--;
        }
    }

    /**
     * exibir a lista preenchida
     */
    public String exibirLista() throws Exception {
        NoDaLista valor = this.primeiro;
        String lista = "{ ";
        if (this.isVazia()) {
            throw new Exception("A lista não pode ser exibida vázia");
        }
        int count = 1; // inicia o contador 
        while (count <= this.getTotal()) {
            lista += Integer.toString(valor.valor) + ",";
            valor = valor.proximo;
            count++;
        }
        lista = lista + "}";
        return lista = lista.replaceAll(",}", "}"); // retorna a lista completa e atualizada 
    }

}
