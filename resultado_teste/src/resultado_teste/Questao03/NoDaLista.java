package resultado_teste.Questao03;

/**
 * baseado na questao 3 ser uma lista encardeada
 *
 * @author igor
 */
public class NoDaLista {

    int valor; // valor da lista 
    NoDaLista proximo; // valor proximo da lista 

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public NoDaLista getProximo() {
        return proximo;
    }

    public void setProximo(NoDaLista proximo) {
        this.proximo = proximo;
    }

    public NoDaLista(int valor, NoDaLista proximo) {
        this.valor = valor;
        this.proximo = proximo;
    }

    public NoDaLista(int v) {
        valor = v;
        proximo = null;
    }

}
