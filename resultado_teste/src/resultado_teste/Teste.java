package resultado_teste;

/**
 * interface que assina o teste
 *
 * @author igor
 */
public interface Teste {

    String getNumeroQuestao();  // retorna o numero da questao 

    String getAnunciado(); // retorna o anunciado 

    void iniciar(); // iniciando o teste 

    void executando(); // executando o testes

}
