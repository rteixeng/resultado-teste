/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resultado_teste.Questao02;

import java.util.Scanner;
import resultado_teste.Teste;

/**
 * questao n 02
 *
 * @author igor
 */
public class Questao02 implements Teste {

    /**
     * metodo que retorna o numero da questao
     *
     * @return o numero da questao
     */
    @Override
    public String getNumeroQuestao() {
        return "02";
    }

    /**
     * metodo que retorna o anunciando
     *
     * @return a questao
     */
    @Override
    public String getAnunciado() {
        return "Escreva um algoritmo que dado um valor de n, imprima uma escala no console com o caracter #. Onde a base e altura da escada seja igual";
    }

    /**
     * metodo que inicia o teste ;
     */
    @Override
    public void iniciar() {
        System.out.println(this.getNumeroQuestao() + ") " + this.getAnunciado());
        System.out.println("");
        System.out.println("Instrução: Será solicitado o tamanho da escada; ");
        System.out.println("Deseja começar? (s/n) ");
        Scanner scan = new Scanner(System.in);
        String linha = scan.nextLine(); //ler a resposta do cliente 
        while (linha.equalsIgnoreCase("s")) { //
            this.executando(); // inicia o teste 
            System.out.println("Deseja começar? (s/n) ");
            linha = scan.nextLine(); //ler a resposta do cliente 
        }
        System.exit(0); // sai do exercicio
    }

    /**
     * executando o teste 02
     */
    @Override
    public void executando() {
        System.out.print("Informe o tamanho da escada:  ");
        Scanner scan = new Scanner(System.in);
        try {
            int linha = Integer.parseInt(scan.nextLine()); //ler a resposta do cliente 
            if (linha < 2) {
                throw new Exception();
            } else {
                this.impressao(linha);
            }
        } catch (Exception es) {
            System.out.println("Não é um número válido! A escada terá no minimo 2 degraus!");
        }
    }

    /**
     * imprimi a escada
     *
     * @param numero
     */
    private void impressao(int numero) {
        String imp = " ";
        System.out.println(imp);
        for (int i = 0; i <= numero - 1; i++) {
            imp = "#" + imp;
            System.out.println(String.format("%" + numero + 2 + "s", imp).replace(' ', ' '));
        }
    }

    public static void main(String[] args) {

        System.out.println("Começando..: ");

        Questao02 q = new Questao02(); // instanciando a classe da questao

        q.iniciar();  //inicando a questao 
    }
}
