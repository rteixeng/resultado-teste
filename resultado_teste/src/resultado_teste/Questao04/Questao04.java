/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resultado_teste.Questao04;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import resultado_teste.Teste;

/**
 * questao n 04
 *
 * @author igor
 */
public class Questao04 implements Teste {

    /**
     * metodo que retorna o numero da questao
     *
     * @return o numero da questao
     */
    @Override
    public String getNumeroQuestao() {
        return "04";
    }

    /**
     * metodo que retorna o anunciando
     *
     * @return a questao
     */
    @Override
    public String getAnunciado() {
        return "Considerando notas de R$ 100, R$ 50, R$ 20, R$ 10, R$ 5, R$ 2. Faça uma rotina que calcule o troco referente a um valor inteiro qualquer. Levando-se em conta que as notas de maior valor devem ser prioridade para o troco. Se possível resolva utilizando recursividade";
    }

    @Override
    public void iniciar() {
        System.out.println(this.getNumeroQuestao() + ") " + this.getAnunciado());
        System.out.println("");
        System.out.println("Instrução: O valor não poderá ser negativo");
        System.out.println("Deseja começar? (s/n) ");
        Scanner scan = new Scanner(System.in);
        String linha = scan.nextLine(); //ler a resposta do cliente 
        while (linha.equalsIgnoreCase("s")) { //
            this.executando(); // inicia o teste 
            System.out.println("Deseja começar? (s/n) ");
            linha = scan.nextLine(); //ler a resposta do cliente 
        }
        System.exit(0); // sai do exercicio

    }

    @Override
    public void executando() {
        this.trocos.clear();
        this.lendoValor();
    }

    /**
     * ler o valor
     */
    private void lendoValor() {
        System.out.println("Informe o valor    "); // sai das opcoes  
        Scanner scan = new Scanner(System.in);
        int valor = Integer.parseInt(scan.nextLine()); // ler o valor e converte para inteiro 
        if (valor < 0) { // valor não poderá ser negativo 
            System.out.println("Valor inválido");
            this.lendoValor();

        }
        this.calculandoTroco(valor, 0);
        System.out.println("Troco: ");
        for (Integer v : this.trocos) {
            System.out.println(" " + v);
        }

    }

    private List<Integer> trocos = new ArrayList<Integer>();

    int[] notas = {100, 50, 20, 10, 5, 2}; // sao as possiveis notas ;

    /**
     * calculando o troco
     *
     * @param valor
     */
    private void calculandoTroco(int valor, int indice) {
        int resto = 0;
        if (indice <= 6 && valor >= notas[indice]) { // verifica se o indice esta no intervalo e se o valor a maior que a  nota apresentada
            resto = valor - notas[indice]; // calcula o valor pagando aquela nota
            this.trocos.add(notas[indice]); // coloca o valor da nota na lista de troco
        } else if (valor < notas[indice]) { // se o valor for menor , verifica na proxima nota
            this.calculandoTroco(valor, indice + 1); // calcula a proxima nota com o valor fornecida
        }
        if (resto > 0.0) { // se ainda existi valores a serem processado 
            this.calculandoTroco(resto, indice + 1); // calcula a proxima nota 
        }
//        return troco;
    }

    public static void main(String[] args) {

        System.out.println("Começando..: ");

        Questao04 q = new Questao04(); // instanciando a classe da questao

        q.iniciar();  //inicando a questao 
    }

}
